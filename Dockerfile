# -- Build Stage --
FROM rust:latest AS builder
WORKDIR /usr/src/universal-order-book-service
COPY . .
RUN rustup component add rustfmt
RUN cargo test
RUN cargo install --path .

# -- Final Stage --
FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/universal-order-book-service /usr/local/bin/universal-order-book-service
CMD ["universal-order-book-service"]
