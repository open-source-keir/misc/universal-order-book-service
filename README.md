# universal-order-book-service
Universal cryptocurrency order book written in Rust. Aggregates crypto exchange order book websocket streams to create a combined view. Serves the data via a gRPC server.

## Development

### Configuration
Configuration is currently handled by a list of environment variables being passed to the docker container. As configuration complexity grows this would be moved into a dedicated .env (or similar) file.

#### Env Vars
Currently, some variables are provided with defaults on startup. These are identified by containing no information in the ENV_KEY column, whilst having a DEFAULT_VALUE.
| ENV_KEY | Description | DEFAULT_VALUE |
| ------ | ------ | ------ |
| RUST_LOG | Logging level | ERROR |
| TICKER_PAIR | OrderBook ticker pair | ethbtc | 
| SOCKET_ADDR | Socket address gRPC is serving on | 127.0.0.1:50052 |
| _ | Binance WebSocket base uri | wss://stream.binance.com:9443/ws |
| _ | Max Binance client consumption rate per second | 4 |
| _ | Bitstamp WebSocket base uri | wss://ws.bitstamp.net/ |
| _ | Max Bitstamp client consumption rate per second | 4 |

### Docker
Currently, building a docker container using the Dockerfile provided is the easiest way to get things running! Each environment variable that you don't want to use the default will need to be passed to the container at run time.

#### Example Build & Run
 1. ```docker build -t universal-order-book-service .```
 2. ```docker run -p 50052:50052 -e SOCKET_ADDR='0.0.0.0:50052' universal-order-book-service```

 ## gRPC API
The universal-order-book-service exposes a Summary of the latest OrderBook state via a gRPC server. This Summary contains the top ten bids & asks, as well as the spread.

### Protobuf Schema
The following schema can be used to generate the appropriate client code to consume the OrderBook Summary data from the gRPC server hosted by this repository. This is the recommended method of programmatic consumption.

```
syntax = "proto3";
package orderbook;

service OrderbookAggregator {
 rpc BookSummary(Empty) returns (stream Summary);
}

message Empty {}

message Summary {
 double spread = 1;
 repeated Level bids = 2;
 repeated Level asks = 3;
}

message Level {
 string exchange = 1;
 double price = 2;
 double amount = 3;
}
```
#### Example universal-order-book gRPC Client
https://gitlab.com/open-source-keir/misc/universal-order-book-client

## Future Improvements
### Client
- Define generic async_trait(s) (eg/ Subscriber & OrderBookGetter) that exchange clients can implement to further de-couple from the service tier.
- Create bespoke subscribe() functions for each client that parses the subscription response immediately after sending the request.
- Add more clients, and add more WebSocket streams.

### Service
- Look into creating a 'DataHandler' abstraction between the client & service tier that enables an arbitrary number of exchange clients to be used without obfuscating the business logic.
- Research into the best ways to optimise the OrderBook stream merging & sorting (ie CPU caches optimisation?).


### General
- Improve Dockerfile build to enable config.rs to load from a JSON file, rather than raw environment variables being passed to 'docker run'. 
- Add CI pipeline that runs the docker build, pushes to Google Container Registry, and deploys to dev/prod.
- Determine how to implement the GRPC service trait in a file.rs that isn't the main executable (eg/ lib/server.rs).
- Flesh out the GRPC 'book_summary' request (ie Empty{} -> Request{ticker_pair, book_depth}) - could assign each request it's own tokio task that ultimately creates a new client connection & consumes the desired OrderBook updates.
- Improve unit & integration test coverage.
- Optimise channel buffer management.


