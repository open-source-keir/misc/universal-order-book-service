fn main() {
    tonic_build::compile_protos("proto/order_book_summary.proto")
        .unwrap_or_else(|err| panic!("Failed to compile .protos {:?}", err));
}