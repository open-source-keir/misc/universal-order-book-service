/// All custom errors generated in the clients module.
pub mod error;

/// Universal OrderBook service.
pub mod universal_order_book;