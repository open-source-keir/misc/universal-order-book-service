use thiserror::Error;
use crate::client::error::ClientError;

/// All custom errors generated in the service module.
#[derive(Error, Debug)]
pub enum ServiceError {
    #[error("Failed to interact with exchange client")]
    ClientInteraction(#[from] ClientError),

    #[error("Failed to parse OrderBook update exchange type due to no first element")]
    OBUpdateArrayEmpty,
}