use crate::client::binance::{OrderBook as BinanceOrderBook, Level as BinanceLevel, Client as BinanceClient};
use crate::client::bitstamp::{OrderBook as BitstampOrderBook, Level as BitstampLevel, Client as BitstampClient};
use crate::service::error::ServiceError;
use async_trait::async_trait;
use tokio::sync::watch;
use tokio::sync::watch::{Sender, Receiver};
use arrayvec::ArrayVec;
use std::iter::FromIterator;
use tokio_stream::StreamExt;
use itertools::Itertools;
use std::cmp::Ordering::Equal;

/// Updates the internal OrderBook.
#[async_trait]
pub trait OrderBookUpdater {
    /// Updates the internal [OrderBook] asynchronously.
    async fn update_order_book(&mut self) -> Result<(), ServiceError>;
}

/// Required components to construct an [OrderBookService] instance.
pub struct Components {
    pub binance: BinanceClient,
    pub bitstamp: BitstampClient,
    pub ticker_pair: String,
}

/// Service that maintains an internal [OrderBook] using input WebSocket streams from
/// Cryptocurrency Exchange clients. Distributes changes to the [OrderBook] state
/// via a tokio::sync::watch::channel.
pub struct OrderBookService {
    binance: BinanceClient,
    bitstamp: BitstampClient,
    ticker_pair: String,
    order_book: OrderBook,
    tx: Sender<OrderBook>,
    rx: Receiver<OrderBook>,
}

#[async_trait]
impl OrderBookUpdater for OrderBookService {
    async fn update_order_book(&mut self) -> Result<(), ServiceError> {
        debug!("OrderBookService.update_order_book() started");

        // Generate OrderBook update streams from exchange clients
        let mut binance = self.binance
            .get_order_book(&self.ticker_pair)
            .await?
            .map(OrderBook::from);

        let mut bitstamp = self.bitstamp
            .get_order_book(&self.ticker_pair)
            .await?
            .map(OrderBook::from);


        // Consume OrderBook updates
        loop {
            let ob_update: OrderBook = tokio::select! {
                Some(update) = binance.next() => update,
                Some(update) = bitstamp.next() => update,
            };

            // Guard
            if ob_update.asks.is_empty() || ob_update.bids.is_empty() {
                continue;
            }

            // Update asks & bids
            self.order_book.asks = self.update_asks(ob_update.asks)?;
            self.order_book.bids = self.update_bids(ob_update.bids)?;

            if self.tx.send(self.order_book.clone()).is_err() {
                info!("All OrderBook update receivers dropped - OrderBookService stopped");
                break;
            }
        }
        Ok(())
    }
}

impl OrderBookService {
    /// Constructs a new [OrderBookService] instance using the provided [Components] struct.
    pub fn new(components: Components) -> Self {
        let (tx, rx) = watch::channel(OrderBook::default());

        Self {
            binance: components.binance,
            bitstamp: components.bitstamp,
            ticker_pair: components.ticker_pair.clone(),
            order_book: OrderBook::default(),
            tx,
            rx,
        }
    }

    /// Returns a [Receiver] handle that can be used to watch changes in the [OrderBook] state.
    pub fn create_order_book_watcher(&mut self) -> Receiver<OrderBook> {
        self.rx.clone()
    }

    /// Returns an updated, ascending-sorted array containing the latest [OrderBook] asks.
    fn update_asks(&self, update: ArrayVec<Level, 10>) -> Result<ArrayVec<Level, 10>, ServiceError> {
        // Both exchanges have an ascending array of ask prices (ie/ best asks first)
        let update_exchange = &update
            .first()
            .ok_or(ServiceError::OBUpdateArrayEmpty)?
            .exchange;

        // Remove elements from the OrderBook that are included in the update
        let mut current_asks = self.order_book.asks.clone();
        current_asks.retain(|level| {
            std::mem::discriminant(&level.exchange) != std::mem::discriminant(update_exchange)
        });

        // Merge & Sort
        Ok(ArrayVec::from_iter(current_asks
            .into_iter()
            .merge_by(update, |left, right| { left.price <= right.price })
            .sorted_by(|left, right| {
                left.price.partial_cmp(&right.price).unwrap_or(Equal)
            })
            .take(10))
        )
    }

    /// Returns an updated, descending-sorted array containing the latest [OrderBook] asks.
    fn update_bids(&mut self, update: ArrayVec<Level, 10>) -> Result<ArrayVec<Level, 10>, ServiceError> {
        // Both exchanges have an descending array of bid prices (ie/ best bids first)
        let update_exchange = &update
            .first()
            .ok_or(ServiceError::OBUpdateArrayEmpty)?
            .exchange;

        // Remove elements from the OrderBook that are included in the update
        let mut current_bids = self.order_book.bids.clone();
        current_bids.retain(|level| {
            std::mem::discriminant(&level.exchange) != std::mem::discriminant(update_exchange)
        });

        // Merge & Sort
        Ok(ArrayVec::from_iter(current_bids
            .into_iter()
            .merge_by(update, |left, right| { left.price >= right.price })
            .sorted_by(|left, right| {
                right.price.partial_cmp(&left.price).unwrap_or(Equal)
            })
            .take(10))
        )
    }
}

/// Universal OrderBook constructed from a fixed-size 10-length [ArrayVec] of [Level]s.
#[derive(Debug, Default, Clone)]
pub struct OrderBook {
    pub asks: ArrayVec<Level, 10>,
    pub bids: ArrayVec<Level, 10>,
}

/// Universal [OrderBook] Level, providing detail on the price, amount, and originating [Exchange].
#[derive(Debug, Clone)]
pub struct Level {
    pub exchange: Exchange,
    pub price: f64,
    pub amount: f64,
}

impl Default for Level {
    fn default() -> Self {
        Self {
            exchange: Exchange::Binance,
            price: 0.0,
            amount: 0.0
        }
    }
}

/// Originating Exchange of a particular [OrderBook] [Level].
#[derive(Debug, Clone)]
pub enum Exchange {
    Binance,
    Bitstamp,
}

impl From<BinanceOrderBook> for OrderBook {
    fn from(binance_ob: BinanceOrderBook) -> Self {
        Self {
            asks: ArrayVec::from_iter(binance_ob.asks.into_iter().take(10).map(Level::from)),
            bids: ArrayVec::from_iter(binance_ob.bids.into_iter().take(10).map(Level::from)),
        }
    }
}

impl From<BinanceLevel> for Level {
    fn from(binance_lvl: BinanceLevel) -> Self {
        Self {
            exchange: Exchange::Binance,
            price: binance_lvl.price,
            amount: binance_lvl.amount,
        }
    }
}

impl From<BitstampOrderBook> for OrderBook {
    fn from(bitstamp_ob: BitstampOrderBook) -> Self {
        Self {
            asks: ArrayVec::from_iter(bitstamp_ob.asks.into_iter().take(10).map(Level::from)),
            bids: ArrayVec::from_iter(bitstamp_ob.bids.into_iter().take(10).map(Level::from)),
        }
    }
}

impl From<BitstampLevel> for Level {
    fn from(bitstamp_lvl: BitstampLevel) -> Self {
        Self {
            exchange: Exchange::Bitstamp,
            price: bitstamp_lvl.price,
            amount: bitstamp_lvl.amount,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::service::universal_order_book::Exchange::{Binance, Bitstamp};

    fn gen_binance_level_vec_of_length(len: usize) -> Vec<BinanceLevel> {
        let mut vec = Vec::with_capacity(len);
        for _ in 1..len {
            vec.push(BinanceLevel::default());
        }
        vec
    }

    fn gen_bitstamp_level_vec_of_length(len: usize) -> Vec<BitstampLevel> {
        let mut vec = Vec::with_capacity(len);
        for _ in 1..len {
            vec.push(BitstampLevel::default());
        }
        vec
    }

    fn gen_lvls(seeds: Vec<(Exchange, i32)>) -> ArrayVec<Level, 10> {
        assert_eq!(seeds.len(), 10);
        ArrayVec::from_iter(
            seeds.into_iter().map(|seed| gen_level(seed.0, seed.1))
        )
    }

    fn gen_level(exchange: Exchange, price: i32) -> Level {
        Level { exchange, price: price as f64, amount: 100.0 }
    }

    #[test]
    fn test_order_book_array_vec_construction_can_never_panic_from_binance() {
        struct TestCase {
            input_binance_ob: BinanceOrderBook,
        }

        let test_cases = vec! [
            TestCase { // Test case 0: bids.len() & asks.len() are < ArrayVec.capacity()
                input_binance_ob: BinanceOrderBook {
                    last_update_id: 1,
                    bids: gen_binance_level_vec_of_length(3),
                    asks: gen_binance_level_vec_of_length(3),
                }
            },
            TestCase { // Test case 1: bids.len() & asks.len() are > ArrayVec.capacity()
                input_binance_ob: BinanceOrderBook {
                    last_update_id: 1,
                    bids: gen_binance_level_vec_of_length(11),
                    asks: gen_binance_level_vec_of_length(11),
                }
            },
            TestCase { // Test case 2: bids.len() & asks.len() are == ArrayVec.capacity()
                input_binance_ob: BinanceOrderBook {
                    last_update_id: 1,
                    bids: gen_binance_level_vec_of_length(10),
                    asks: gen_binance_level_vec_of_length(10),
                }
            },
        ];

        for (index, test) in test_cases.into_iter().enumerate() {
            print!("Test Case: {:?}", index);
            OrderBook::from(test.input_binance_ob);
            println!(" -- SUCCESS")
        }
    }

    #[test]
    fn test_order_book_array_vec_construction_can_never_panic_from_bitstamp() {
        struct TestCase {
            input_binance_ob: BitstampOrderBook,
        }

        let test_cases = vec! [
            TestCase { // Test case 0: bids.len() & asks.len() are < ArrayVec.capacity()
                input_binance_ob: BitstampOrderBook {
                    timestamp: "".to_string(),
                    timestamp_micro: "".to_string(),
                    bids: gen_bitstamp_level_vec_of_length(3),
                    asks: gen_bitstamp_level_vec_of_length(3),
                }
            },
            TestCase { // Test case 1: bids.len() & asks.len() are > ArrayVec.capacity()
                input_binance_ob: BitstampOrderBook {
                    timestamp: "".to_string(),
                    timestamp_micro: "".to_string(),
                    bids: gen_bitstamp_level_vec_of_length(11),
                    asks: gen_bitstamp_level_vec_of_length(11),
                }
            },
            TestCase { // Test case 2: bids.len() & asks.len() are == ArrayVec.capacity()
                input_binance_ob: BitstampOrderBook {
                    timestamp: "".to_string(),
                    timestamp_micro: "".to_string(),
                    bids: gen_bitstamp_level_vec_of_length(10),
                    asks: gen_bitstamp_level_vec_of_length(10),
                }
            },
        ];

        for (index, test) in test_cases.into_iter().enumerate() {
            print!("Test Case: {:?}", index);
            OrderBook::from(test.input_binance_ob);
            println!(" -- SUCCESS")
        }
    }

    #[test]
    fn test_update_asks() {
        let mut service = OrderBookService::new(Components {
            binance: BinanceClient::default(),
            bitstamp: BitstampClient::default(),
            ticker_pair: "ethbtc".to_string()
        });

        struct TestCase {
            current: ArrayVec<Level, 10>,
            update: ArrayVec<Level, 10>,
            expected: ArrayVec<Level, 10>,
        }

        let test_cases = vec![
            // Test case 0: All of update array is better, so it replaces every element
            TestCase {
                current: gen_lvls(vec![(Binance, 11), (Bitstamp, 12), (Bitstamp, 13), (Binance, 14), (Binance, 15), (Bitstamp, 16), (Bitstamp, 17), (Binance, 18), (Binance, 19), (Binance, 20)]),
                update: gen_lvls(vec![(Binance, 1), (Binance, 2), (Binance, 3), (Binance, 4), (Binance, 5), (Binance, 6), (Binance, 7), (Binance, 8), (Binance, 9), (Binance, 10)]),
                expected: gen_lvls(vec![(Binance, 1), (Binance, 2), (Binance, 3), (Binance, 4), (Binance, 5), (Binance, 6), (Binance, 7), (Binance, 8), (Binance, 9), (Binance, 10)]),
            },
            // Test case 1: Most of update array is better, but some are the same
            TestCase {
                current: gen_lvls(vec![(Binance, 11), (Bitstamp, 12), (Bitstamp, 13), (Binance, 14), (Binance, 15), (Bitstamp, 16), (Bitstamp, 17), (Binance, 18), (Binance, 19), (Binance, 20)]),
                update: gen_lvls(vec![(Binance, 1), (Binance, 2), (Binance, 3), (Binance, 4), (Binance, 5), (Binance, 12), (Binance, 13), (Binance, 20), (Binance, 21), (Binance, 22)]),
                expected: gen_lvls(vec![(Binance, 1), (Binance, 2), (Binance, 3), (Binance, 4), (Binance, 5), (Bitstamp, 12), (Binance, 12), (Bitstamp, 13), (Binance, 13), (Bitstamp, 16)]),
            },
            // Test case 2: All of update is worse, but need to update outdated entries
            TestCase {
                current: gen_lvls(vec![(Binance, 11), (Bitstamp, 12), (Bitstamp, 13), (Binance, 14), (Binance, 15), (Bitstamp, 16), (Bitstamp, 17), (Binance, 18), (Binance, 19), (Binance, 20)]),
                update: gen_lvls(vec![(Binance, 21), (Binance, 22), (Binance, 23), (Binance, 24), (Binance, 25), (Binance, 26), (Binance, 27), (Binance, 28), (Binance, 29), (Binance, 30)]),
                expected: gen_lvls(vec![(Bitstamp, 12), (Bitstamp, 13), (Bitstamp, 16), (Bitstamp, 17), (Binance, 21), (Binance, 22), (Binance, 23), (Binance, 24), (Binance, 25), (Binance, 26)]),
            },
            // Test case 3: Some of update is worse, but need to update outdated entries and insert correctly
            TestCase {
                current: gen_lvls(vec![(Binance, 11), (Bitstamp, 12), (Bitstamp, 13), (Binance, 14), (Binance, 15), (Bitstamp, 16), (Bitstamp, 17), (Binance, 18), (Binance, 19), (Binance, 20)]),
                update: gen_lvls(vec![(Binance, 1), (Binance, 2), (Binance, 3), (Binance, 4), (Binance, 5), (Binance, 21), (Binance, 22), (Binance, 23), (Binance, 24), (Binance, 25)]),
                expected: gen_lvls(vec![(Binance, 1), (Binance, 2), (Binance, 3), (Binance, 4), (Binance, 5), (Bitstamp, 12), (Bitstamp, 13), (Bitstamp, 16), (Bitstamp, 17), (Binance, 21)]),
            },
        ];

        for (test_index, test) in test_cases.into_iter().enumerate() {
            // Set service asks to test current asks
            service.order_book.asks = test.current;

            // Update with test update asks
            let actual_new_current = service.update_asks(test.update).unwrap();

            for (elem_index, (expected, actual)) in test.expected
                .into_iter()
                .zip(actual_new_current.into_iter())
                .enumerate() {
                assert_eq!(expected.price, actual.price, "Test case: {:?}: Element: {:?}", test_index, elem_index);
            }
        }
    }

    #[test]
    fn test_update_bids() {
        let mut service = OrderBookService::new(Components {
            binance: BinanceClient::default(),
            bitstamp: BitstampClient::default(),
            ticker_pair: "ethbtc".to_string()
        });

        struct TestCase {
            current: ArrayVec<Level, 10>,
            update: ArrayVec<Level, 10>,
            expected: ArrayVec<Level, 10>,
        }

        let test_cases = vec![
            // Test case 0: All of update array is better, so it replaces every element
            TestCase {
                current: gen_lvls(vec![(Binance, 20), (Bitstamp, 19), (Bitstamp, 18), (Binance, 17), (Binance, 16), (Bitstamp, 15), (Bitstamp, 14), (Binance, 13), (Binance, 12), (Binance, 11)]),
                update: gen_lvls(vec![(Binance, 30), (Binance, 29), (Binance, 28), (Binance, 27), (Binance, 26), (Binance, 25), (Binance, 24), (Binance, 23), (Binance, 22), (Binance, 21)]),
                expected: gen_lvls(vec![(Binance, 30), (Binance, 29), (Binance, 28), (Binance, 27), (Binance, 26), (Binance, 25), (Binance, 24), (Binance, 23), (Binance, 22), (Binance, 21)]),
            },
            // Test case 1: Most of update array is better, but some are the same
            TestCase {
                current: gen_lvls(vec![(Binance, 20), (Bitstamp, 19), (Bitstamp, 18), (Binance, 17), (Binance, 16), (Bitstamp, 15), (Bitstamp, 14), (Binance, 13), (Binance, 12), (Binance, 11)]),
                update: gen_lvls(vec![(Binance, 31), (Binance, 30), (Binance, 29), (Binance, 28), (Binance, 19), (Binance, 18), (Binance, 15), (Binance, 10), (Binance, 9), (Binance, 8)]),
                expected: gen_lvls(vec![(Binance, 31), (Binance, 30), (Binance, 29), (Binance, 28), (Bitstamp, 19), (Binance, 19), (Bitstamp, 18), (Binance, 18), (Binance, 15), (Bitstamp, 15)]),
            },
            // Test case 2: All of update is worse, but need to update outdated entries
            TestCase {
                current: gen_lvls(vec![(Binance, 20), (Bitstamp, 19), (Bitstamp, 18), (Binance, 17), (Binance, 16), (Bitstamp, 15), (Bitstamp, 14), (Binance, 13), (Binance, 12), (Binance, 11)]),
                update: gen_lvls(vec![(Binance, 9), (Binance, 8), (Binance, 7), (Binance, 6), (Binance, 5), (Binance, 4), (Binance, 3), (Binance, 2), (Binance, 1), (Binance, 0)]),
                expected: gen_lvls(vec![(Bitstamp, 19), (Bitstamp, 18), (Bitstamp, 15), (Bitstamp, 14), (Binance, 9), (Binance, 8), (Binance, 7), (Binance, 6), (Binance, 5), (Binance, 4)]),
            },
            // Test case 3: Some of update is worse, but need to update outdated entries and insert correctly
            TestCase {
                current: gen_lvls(vec![(Binance, 20), (Bitstamp, 19), (Bitstamp, 18), (Binance, 17), (Binance, 16), (Bitstamp, 15), (Bitstamp, 14), (Binance, 13), (Binance, 12), (Binance, 11)]),
                update: gen_lvls(vec![(Binance, 23), (Binance, 22), (Binance, 21), (Binance, 7), (Binance, 6), (Binance, 5), (Binance, 4), (Binance, 3), (Binance, 2), (Binance, 1)]),
                expected: gen_lvls(vec![(Binance, 23), (Binance, 22), (Binance, 21), (Bitstamp, 19), (Bitstamp, 18), (Bitstamp, 15), (Bitstamp, 14), (Binance, 7), (Binance, 6), (Binance, 5)]),
            },
        ];

        for (test_index, test) in test_cases.into_iter().enumerate() {
            // Set service asks to test current asks
            service.order_book.bids = test.current;

            // Update with test update asks
            let actual_new_current = service.update_bids(test.update).unwrap();

            for (elem_index, (expected, actual)) in test.expected
                .into_iter()
                .zip(actual_new_current.into_iter())
                .enumerate() {
                assert_eq!(expected.price, actual.price, "Test case: {:?}: Element: {:?}", test_index, elem_index);
            }
        }
    }
}