/// Rust WebSocket clients for popular Cryptocurrency Exchanges.
pub mod client;

/// Rust service that utilises exchange WebSocket [client]s to merge and sort multiple
/// OrderBook streams.
pub mod service;

#[macro_use]
extern crate log;