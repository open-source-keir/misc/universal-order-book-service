/// All custom errors generated in the client module.
pub mod error;

/// Binance Exchange WebSocket client
pub mod binance;

/// Bitstamp Exchange WebSocket client.
pub mod bitstamp;

use crate::client::error::ClientError;
use serde::{Serialize, Deserialize, Deserializer, de};
use tokio_tungstenite::{connect_async, WebSocketStream, MaybeTlsStream};
use tokio_tungstenite::tungstenite::{Message as WSMessage};
use std::str::FromStr;
use tokio::net::TcpStream;
use futures_util::SinkExt;

type WSStream = WebSocketStream<MaybeTlsStream<TcpStream>>;

/// Utilised to subscribe to an Exchange [WebSocketStream] stream (eg/ OrderBook stream).
pub trait Subscription {
    /// Constructs a new [Subscription] implementation.
    fn new(stream_name: String, ticker_pair: String) -> Self;
    /// Serializes the [Subscription] implementation in a String data format.
    fn as_text(&self) -> Result<String, ClientError> where Self: Serialize {
        Ok(serde_json::to_string(self)?)
    }
}

/// Generic configuration for Exchange Clients.
#[derive(Debug)]
pub struct Config {
    /// Base URI of the Exchange WebSocket endpoint.
    pub base_uri: String,
    /// Maximum consumption rate of a Client.
    pub rate_limit_per_second: u64,
}

/// Connect asynchronously to an Exchange's server, returning a [WebSocketStream].
async fn connect(base_uri: &String) -> Result<WSStream, ClientError> {
    connect_async(base_uri)
        .await
        .and_then(|(ws_stream, _)| Ok(ws_stream))
        .map_err(|err| ClientError::WebSocketConnect(err))
}

/// Subscribe asynchronously to a WebSocket data stream using the [Subscription] provided.
async fn subscribe<Sub>(mut ws_conn: WSStream, subscription: Sub) -> Result<WSStream, ClientError>
where
    Sub: Subscription + Serialize,
{
    ws_conn
        .send(WSMessage::text(subscription.as_text()?))
        .await
        .map_err(|write_err| ClientError::WebSocketWrite(write_err))?;

    Ok(ws_conn)
}

/// Custom [Deserializer] function to deserialize an input [str] to a [f64].
pub fn de_str_to_f64<'de, D>(deserializer: D) -> Result<f64, D::Error> where D: Deserializer<'de> {
    let input_str: &str = Deserialize::deserialize(deserializer)?;
    f64::from_str(input_str).map_err(de::Error::custom)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::client::binance::Sub as BinanceSub;
    use crate::client::bitstamp::Sub as BitstampSub;

    async fn gen_binance_conn() -> WSStream {
        connect(&String::from("wss://stream.binance.com:9443/ws")).await.unwrap()
    }

    fn gen_valid_binance_sub() -> BinanceSub {
        BinanceSub::new("@depth20@100ms".to_string(), "ethbtc".to_string())
    }

    fn gen_valid_bitstamp_sub() -> BitstampSub {
        BitstampSub::new("order_book_".to_string(), "ethbtc".to_string())
    }

    #[tokio::test]
    async fn test_connect() {
        struct TestCase {
            input_base_uri: String,
            expected_can_connect: bool,
        }

        let test_cases = vec![
            TestCase { // Test case 0: Not a valid WS base URI
                input_base_uri: "not a valid base uri".to_string(),
                expected_can_connect: false,
            },
            TestCase { // Test case 1: Valid Binance WS base URI
                input_base_uri: "wss://stream.binance.com:9443/ws".to_string(),
                expected_can_connect: true,
            },
            TestCase { // Test case 2: Valid Bitstamp WS base URI
                input_base_uri: "wss://ws.bitstamp.net/".to_string(),
                expected_can_connect: true
            },
        ];

        for (index, test) in test_cases.into_iter().enumerate() {
            let actual_result = connect(&test.input_base_uri).await;
            assert_eq!(test.expected_can_connect, actual_result.is_ok(), "Test case: {:?}", index);
        }
    }

    #[tokio::test]
    async fn test_binance_subscribe() {
        struct TestCase {
            input_conn: WSStream,
            input_sub: BinanceSub,
            expected_can_subscribe: bool,
        }

        let test_cases = vec![
            TestCase { // Test case 0: Binance subscription
                input_conn: gen_binance_conn().await,
                input_sub: gen_valid_binance_sub(),
                expected_can_subscribe: true,
            },
        ];

        for (index, test) in test_cases.into_iter().enumerate() {
            println!("{:?}", test.input_sub);
            let actual_result = subscribe(
                test.input_conn, test.input_sub).await;
            assert_eq!(test.expected_can_subscribe, actual_result.is_ok(), "Test case: {:?}", index);
        }
    }

    #[tokio::test]
    async fn test_bitstamp_subscribe() {
        struct TestCase {
            input_conn: WSStream,
            input_sub: BitstampSub,
            expected_can_subscribe: bool,
        }

        let test_cases = vec![
            TestCase { // Test case 0: Bitstamp subscription
                input_conn: gen_binance_conn().await,
                input_sub: gen_valid_bitstamp_sub(),
                expected_can_subscribe: true,
            },
        ];

        for (index, test) in test_cases.into_iter().enumerate() {
            let actual_result = subscribe(
                test.input_conn, test.input_sub).await;
            assert_eq!(test.expected_can_subscribe, actual_result.is_ok(), "Test case: {:?}", index);
        }
    }
}