use crate::client::{WSMessage, Config, Subscription, connect, subscribe, de_str_to_f64};
use crate::client::error::ClientError;
use serde::{Deserialize, Serialize};
use std::time::Duration;
use tokio_tungstenite::tungstenite;
use tokio_stream::wrappers::ReceiverStream;
use tokio::sync::mpsc;
use tokio_stream::StreamExt;
use std::collections::HashMap;
use std::iter::FromIterator;
use std::array::IntoIter;

/// Bitstamp Client containing client specific configuration.
#[derive(Debug, Default)]
pub struct Client {
    base_uri: String,
    rate_limit_interval: Duration,
}

impl Client {
    const ORDER_BOOK_STREAM: &'static str = "order_book_";

    /// Constructs a new [Client] instance using the provided [Config] struct.
    pub fn new(cfg: &Config) -> Self {
        Self {
            base_uri: cfg.base_uri.clone(),
            rate_limit_interval: Duration::from_millis(1000 / cfg.rate_limit_per_second),
        }
    }

    /// Consumes the 'Live Order Book' stream, returning a [ReceiverStream] containing the latest
    /// view of the Bitstamp [OrderBook].
    pub async fn get_order_book(&self, ticker_pair: &String) -> Result<ReceiverStream<OrderBook>, ClientError> {
        // Create buffered channel to transmit order book data
        let (tx, rx) = mpsc::channel(100);

        // Connect to Bitstamp WebSocket Server
        let ws_conn = connect(&self.base_uri).await?;

        // Subscribe to OrderBook stream
        let sub = Sub::new(String::from(Client::ORDER_BOOK_STREAM), ticker_pair.clone());
        let mut stream = subscribe(ws_conn, sub)
            .await?
            .filter_map(Client::map_to_bitstamp_result);

        // Create rate limiting interval
        let mut rate_limit = tokio::time::interval(self.rate_limit_interval.clone());

        // Consume order book stream & transmit relevant data to receiver
        tokio::spawn(async move {
            while let Some(message_result) = stream.next().await {


                // Rate Limit consumption
                rate_limit.tick().await;

                // Handle Bitstamp Result<Message, ClientError>
                let message = match message_result {
                    Ok(message) => message,
                    Err(err) => {
                        warn!("Skipping message Result due to unexpected error: {:?}", err);
                        continue;
                    }
                };

                // Handle Bitstamp Message
                match message {
                    Message::Data(data) => {
                        if let Err(err) = tx.send(data.order_book).await {
                            info!("Consumer of Client::get_order_book dropped the receiver - closing stream: {:?}", err);
                            return
                        }
                    },
                    Message::Subscription(_) => {
                        info!("Bitstamp Subscription success!");
                    }
                }
            }
        });

        Ok(ReceiverStream::new(rx))
    }

    // Maps a Result<tungstenite::Message, tungstenite::Error> into a Bitstamp
    // Result<Message, ClientError>
    fn map_to_bitstamp_result(ws_result: Result<WSMessage, tungstenite::Error>) -> Option<Result<Message, ClientError>> {
        let ws_message = match ws_result {
            Ok(ws_message) => ws_message,
            Err(ws_err) => return Some(Err(ClientError::WebSocketRead(ws_err)))
        };

        match ws_message {
            WSMessage::Text(text) => {
                match serde_json::from_str(&*text) {
                    Ok(message) => Some(Ok(message)),
                    Err(err) => Some(Err(ClientError::Deserialisation(err)))
                }
            }
            _ => None
        }
    }
}

/// Bitstamp specific subscription message.
#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Sub {
    event: String,
    data: HashMap<String, String>,
}

impl Subscription for Sub {
    fn new(stream_name: String, ticker_pair: String) -> Self {
        Self {
            event: String::from("bts:subscribe"),
            data: HashMap::<String, String>::from_iter(IntoIter::new(
                [(String::from("channel"), format!("{}{}", stream_name, ticker_pair))]))
        }
    }
}

/// Bitstamp specific subscription success response message.
#[derive(Debug, Deserialize, Serialize)]
pub struct SubscriptionSuccess {
    event: String,
    channel: String,
    data: HashMap<String, String>,
}

/// Message variants that could be received from Bitstamp WebSocket server.
#[derive(Debug, Deserialize, Serialize)]
#[serde(untagged)]
pub enum Message {
    Data(Data),
    Subscription(Sub),
}

/// Data related [Message] variant value, consumed via [Client::get_order_book], containing an
/// [OrderBook].
#[derive(Debug, Deserialize, Serialize)]
pub struct Data {
    event: String,
    channel: String,
    #[serde(rename = "data")]
    order_book: OrderBook,
}

/// OrderBook data model outputted by Bitstamp, consumed via [Client::get_order_book].
#[derive(Debug, Default, Deserialize, Serialize)]
pub struct OrderBook {
    pub timestamp: String,
    #[serde(rename = "microtimestamp")]
    pub timestamp_micro: String,
    pub bids: Vec<Level>,
    pub asks: Vec<Level>,
}

/// Level data model outputted by Bitstamp, used to construct the [OrderBook] consumed via
/// [Client::get_order_book].
#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Level {
    #[serde(deserialize_with = "de_str_to_f64")]
    pub price: f64,
    #[serde(deserialize_with = "de_str_to_f64")]
    pub amount: f64,
}

#[cfg(test)]
mod tests {
    use super::*;

    fn gen_valid_bitstamp_message_string() -> String {
        "{\"event\": \"bts:subscription_succeeded\", \"data\": {}}".to_string()
    }

    #[test]
    fn test_map_to_bitstamp_result() {
        struct TestCase {
            input_ws_result: Result<WSMessage, tungstenite::Error>,
            expected_output: Option<Result<(), ()>>,
        }

        let test_cases = vec![
            TestCase { // Test case 0: Ok(WSMessage::Ping) -> None
                input_ws_result: Ok(WSMessage::Ping(vec![0])),
                expected_output: None,
            },
            TestCase { // Test case 1: Ok(WSMessage::Pong) -> None
                input_ws_result: Ok(WSMessage::Pong(vec![0])),
                expected_output: None,
            },
            TestCase { // Test case 2: Ok(WSMessage::Text(!parsable) -> Some(ClientError)
                input_ws_result: Ok(WSMessage::Text("not parsable".to_string())),
                expected_output: Some(Err(())),
            },
            TestCase { // Test case 3: Ok(WSMessage::Text(parsable) -> Some(Ok)
                input_ws_result: Ok(WSMessage::Text(gen_valid_bitstamp_message_string())),
                expected_output: Some(Ok(())),
            },
        ];

        for (index, test) in test_cases.into_iter().enumerate() {
            let actual_result = Client::map_to_bitstamp_result(test.input_ws_result);

            if test.expected_output.is_none() {
                assert_eq!(test.expected_output.is_none(), actual_result.is_none(), "Test case: {:?}", index);
            } else {
                println!("{:?}", actual_result);
                assert_eq!(test.expected_output.unwrap().is_ok(), actual_result.unwrap().is_ok(), "Test case: {:?}", index);
            }
        }
    }
}