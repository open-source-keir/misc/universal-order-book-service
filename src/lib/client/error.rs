use thiserror::Error;
use tokio_tungstenite::tungstenite;

/// All custom errors generated in the client module.
#[derive(Error, Debug)]
pub enum ClientError {
    #[error("Failed to establish connection via websocket handshake")]
    WebSocketConnect(#[source] tungstenite::error::Error),

    #[error("Failed to write data via websocket connection")]
    WebSocketWrite(#[source] tungstenite::error::Error),

    #[error("Failed to read data via websocket connection")]
    WebSocketRead(#[source] tungstenite::error::Error),

    #[error("Failed to deserialise message contents")]
    Deserialisation(#[from] serde_json::Error),
}

