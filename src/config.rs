use common::client::Config as ClientConfig;
use std::env;
use std::fmt::Debug;

// Ticker Pair
const TICKER_PAIR: &str = "TICKER_PAIR";
const DEFAULT_TICKER_PAIR: &str = "ethbtc";

// Binance Client
const DEFAULT_BINANCE_BASE_URI: &str = "wss://stream.binance.com:9443/ws";
const DEFAULT_BINANCE_RATE_LIMIT: u64 = 4;

// Bitstamp Client
const DEFAULT_BITSTAMP_BASE_URI: &str = "wss://ws.bitstamp.net/";
const DEFAULT_BITSTAMP_RATE_LIMIT: u64 = 4;

// Server Socket
const SOCKET_ADDR: &str = "SOCKET_ADDR";
const DEFAULT_SOCKET_ADDR: &str = "127.0.0.1:50052";

// Overall configuration for the service
#[derive(Debug)]
pub struct Config {
    pub ticker_pair: String,
    pub binance: ClientConfig,
    pub bitstamp: ClientConfig,
    pub server: ServerConfig,
}

// Server specific configuration
#[derive(Debug)]
pub struct ServerConfig {
    pub socket_addr: String,
}

// Load the environment configuration, using default values where required
pub fn load_config() -> Config {
    let cfg = Config {
        binance: ClientConfig {
            base_uri: String::from(DEFAULT_BINANCE_BASE_URI),
            rate_limit_per_second: DEFAULT_BINANCE_RATE_LIMIT,
        },
        bitstamp: ClientConfig {
            base_uri: String::from(DEFAULT_BITSTAMP_BASE_URI),
            rate_limit_per_second: DEFAULT_BITSTAMP_RATE_LIMIT
        },
        ticker_pair: fetch_env_var(TICKER_PAIR, DEFAULT_TICKER_PAIR),
        server: ServerConfig {
            socket_addr: fetch_env_var(SOCKET_ADDR, DEFAULT_SOCKET_ADDR),
        },
    };
    debug!("Environment configuration loaded: {:?}", cfg);
    cfg
}

// Fetch an environment variable, using the provided default value if not found
fn fetch_env_var(key: &str, default_value: &str) -> String {
    match env::var(key) {
        Ok(value) => {
            debug!("Fetched env variable {:?}: {:?}", key, value);
            value
        }
        Err(_) => {
            debug!("Undefined env variable {:?}, using default: {:?}", key, default_value);
            String::from(default_value)
        }
    }
}