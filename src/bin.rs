mod config;
pub mod order_book_summary {
    tonic::include_proto!("order_book_summary");
}
use order_book_summary::{Empty, Level, Summary};
use order_book_summary::order_book_aggregator_server::{OrderBookAggregator, OrderBookAggregatorServer};

#[macro_use]
extern crate log;

use crate::config::load_config;
use common::client::binance::Client as BinanceClient;
use common::client::bitstamp::Client as BitstampClient;
use common::service::universal_order_book::{OrderBookService, Components, OrderBookUpdater, OrderBook, Level as ServiceLevel, Exchange};
use log::{debug, info};
use tokio::sync::{mpsc, watch};
use tokio_stream::wrappers::ReceiverStream;
use tonic::{Request, Response, Status};
use tonic::transport::Server;
use std::iter::FromIterator;

#[tokio::main]
async fn main() {
    // Initialise logger
    init_logger();

    // Load configuration
    let cfg = load_config();

    // Construct Exchange Clients
    let binance = BinanceClient::new(&cfg.binance);
    let bitstamp = BitstampClient::new(&cfg.bitstamp);

    // Construct & Service
    let mut service = OrderBookService::new(Components {
        binance,
        bitstamp,
        ticker_pair: cfg.ticker_pair.clone(),
    });

    // Obtain an OrderBookService watch::Receiver to observe changes in OrderBook state
    let watch_rx = service.create_order_book_watcher();

    // Run OrderBookService
    tokio::spawn(async move {
        if let Err(err) = service.update_order_book().await {
            error!("OrderBookService Error: {:?}", err);
            return
        }
    });

    // Build & Run Server
    let socket = cfg.server.socket_addr
        .parse()
        .unwrap_or_else(|_| panic!("SocketAddr could not be parsed: {:?}", cfg.server.socket_addr));

    info!("Starting gRPC Server running on socket: {:?}", socket);
    Server::builder()
        .add_service(OrderBookAggregatorServer::new(OrderBookServer::new(watch_rx)))
        .serve(socket)
        .await
        .unwrap_or_else(|_| panic!("Could not serve on SocketAddr: {:?}", socket));
}

fn init_logger() {
    env_logger::init();
    debug!("Initialised logger")
}

// OrderBook Server that implements the GRPC OrderBookAggregator service.
// Uses an watch::Receiver clone from the OrderBookService to stream OrderBook
// updates to the server on demand.
pub struct OrderBookServer {
    pub book_watch_rx: watch::Receiver<OrderBook>,
}

#[tonic::async_trait]
impl OrderBookAggregator for OrderBookServer {

    type BookSummaryStream = ReceiverStream<Result<Summary, Status>>;

    async fn book_summary(&self, _: Request<Empty>) -> Result<Response<Self::BookSummaryStream>, Status> {
        info!("Server-Side BookSummaryStream request from Client accepted: streaming...");

        // Create channel
        let (response_tx, response_rx) = mpsc::channel(100);

        // Clone OrderBook update watch receiver
        let mut book_watch_rx = self.book_watch_rx.clone();

        // Send OrderBook Summary updates to Response channel
        tokio::spawn(async move {

            // Detect OrderBook updates
            while book_watch_rx.changed().await.is_ok() {

                // Clone latest OrderBook
                let order_book = book_watch_rx.borrow().clone();
                debug!("OrderBook received from OrderBookService: {:?}", order_book);

                // Generate Summary
                let summary = Summary::from(order_book);

                if response_tx.send(Ok(summary)).await.is_err() {
                    info!("Client dropped Server-Side BookSummaryStream - stream closed");
                    return
                }
            }
        });

        Ok(Response::new(ReceiverStream::new(response_rx)))
    }

}

impl OrderBookServer {
    // Constructs a new OrderBookServer instance using the OrderBookService
    // watch::Receiver<OrderBook> handle.
    pub fn new(book_watch_rx: watch::Receiver<OrderBook>) -> Self {
        Self {
            book_watch_rx
        }
    }
}

impl From<OrderBook> for Summary {
    fn from(order_book: OrderBook) -> Self {
        Self {
            spread: calculate_spread(order_book.asks.first(), order_book.bids.first()),
            asks: Vec::from_iter(order_book.asks.into_iter().take(10).map(Level::from)),
            bids: Vec::from_iter(order_book.bids.into_iter().take(10).map(Level::from)),
        }
    }
}

impl From<ServiceLevel> for Level {
    fn from(service_lvl: ServiceLevel) -> Self {
        let exchange = match service_lvl.exchange {
            Exchange::Binance => String::from("binance"),
            Exchange::Bitstamp => String::from("bitstamp"),
        };

        Self {
            exchange,
            price: service_lvl.price,
            amount: service_lvl.amount,
        }
    }
}

fn calculate_spread(best_ask: Option<&ServiceLevel>, best_bid: Option<&ServiceLevel>) -> f64 {
    match (best_ask, best_bid) {
        (Some(best_ask), Some(best_bid)) if best_ask.price >= best_bid.price => {
            best_ask.price - best_bid.price
        },
        (Some(best_ask), Some(best_bid)) if best_ask.price < best_bid.price => {
            debug!("Calculated spread with very close or out of order bids & asks - using spread.abs()");
            (best_ask.price - best_bid.price).abs()
        }
        _ => {
            warn!("Failed to calculate spread - OrderBook bids and/or asks first element was None");
            0.0
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn gen_service_level_with_price(price: f64) -> ServiceLevel {
        ServiceLevel { exchange: Exchange::Binance, price, amount: 10.0 }
    }

    #[test]
    fn test_calculate_spread() {
        struct TestCase {
            input_best_ask: Option<ServiceLevel>,
            input_best_bid: Option<ServiceLevel>,
            expected_spread: f64,
        }

        let test_cases = vec![
            TestCase { // Test case 0: Input ask & bid None
                input_best_ask: None,
                input_best_bid: None,
                expected_spread: 0.0
            },
            TestCase { // Test case 1: Input ask None & bid Some
                input_best_ask: None,
                input_best_bid: Some(ServiceLevel::default()),
                expected_spread: 0.0
            },
            TestCase { // Test case 2: Input ask Some & bid None
                input_best_ask: Some(ServiceLevel::default()),
                input_best_bid: None,
                expected_spread: 0.0
            },
            TestCase { // Test case 3: Input ask & bid Some
                input_best_ask: Some(gen_service_level_with_price(100.0)),
                input_best_bid: Some(gen_service_level_with_price(90.0)),
                expected_spread: 10.0
            },
            TestCase { // Test case 4: Input ask & bid Some
                input_best_ask: Some(gen_service_level_with_price(0.068411)),
                input_best_bid: Some(gen_service_level_with_price(0.06841198)),
                expected_spread: ((0.068411 - 0.06841198) as f64) .abs()
            },
        ];

        for (index, test) in test_cases.into_iter().enumerate() {
            let actual_spread = calculate_spread(
                test.input_best_ask.as_ref(), test.input_best_bid.as_ref()
            );
            assert_eq!(test.expected_spread, actual_spread, "Test case: {:?}", index);
        }
    }
}